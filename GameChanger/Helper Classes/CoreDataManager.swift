//
//  CoreDataManager.swift
//  GameChanger
//
//  Created by Gaurav Luthra on 2/29/20.
//  Copyright © 2020 Gaurav Luthra. All rights reserved.
//

import Foundation
import CoreData

class CoreDataManager{
    static let shared = CoreDataManager()
    private init(){}
    
    func refreshData(issues:[Issue]){
        deleteOldData()
        saveIssues(issues:issues)
    }
    
    private func saveIssues(issues:[Issue]){
        let managedContext = AppDelegate.shared.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName:"IssueObjectList",in: managedContext)!
        let issueList = NSManagedObject(entity:entity,insertInto: managedContext) as! IssueObjectList
        issueList.issuesData = try? JSONEncoder().encode(issues)
        issueList.savedAt = Int64(Date().timeIntervalSince1970)
        do{
            try managedContext.save()
            print("Save issues to Core Data on:\(issueList.savedAt)")
        }catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    func getSavedIssues() -> IssuesDetail{
        let managedContext = AppDelegate.shared.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "IssueObjectList")
        var allIssues = [Issue]()
        var timeStamp:Int64?
        do {
            let issueList = (try managedContext.fetch(fetchRequest) as! [IssueObjectList]).first
            if let issuesData = issueList?.issuesData{
                allIssues = try! JSONDecoder().decode([Issue].self, from:issuesData)
                timeStamp  = issueList?.savedAt
            }
            print("fetched data:")
        }catch let error as NSError {
          print("Could not fetch. \(error), \(error.userInfo)")
        }
        return (allIssues,timeStamp)
        
    }
    
    func deleteOldData(){
        let managedContext = AppDelegate.shared.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName:"IssueObjectList")
        do {
            let issuesList = try managedContext.fetch(fetchRequest) as! [IssueObjectList]
            for aIssueList in issuesList{
                managedContext.delete(aIssueList)
            }
            print("Deleted data")
        } catch let error as NSError {
          print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
}
