//
//  IssuesProtocol.swift
//  GameChanger
//
//  Created by Gaurav Luthra on 2/29/20.
//  Copyright © 2020 Gaurav Luthra. All rights reserved.
//

import Foundation

typealias IssuesDetail = (issues:[Issue],timestamp:Int64?)

protocol IssuesValidityChecker {}
extension IssuesValidityChecker{
    
    func getValidIssues() -> [Issue]{
        let savedData = CoreDataManager.shared.getSavedIssues()
        let savedTimestamp = savedData.timestamp
        let currentTimestamp = Int64(Date().timeIntervalSince1970)
        if savedData.issues.isEmpty { return [] }
        if let savedTS = savedTimestamp,(currentTimestamp - savedTS) >= 86400 { return [] }
        return savedData.issues
    }
    
}
