//
//  IssuesTableView.swift
//  GameChanger
//
//  Created by Gaurav Luthra on 2/29/20.
//  Copyright © 2020 Gaurav Luthra. All rights reserved.
//

import Foundation
import UIKit

class IssuesTableView: UITableView {
    override func awakeFromNib() {
        super.awakeFromNib()
        self.rowHeight = UITableView.automaticDimension
        self.estimatedRowHeight = 100
        self.tableFooterView = UIView(frame: .zero)
    }
}
