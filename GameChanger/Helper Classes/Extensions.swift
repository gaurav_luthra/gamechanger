//
//  Extensions.swift
//  GameChanger
//
//  Created by Gaurav Luthra on 2/29/20.
//  Copyright © 2020 Gaurav Luthra. All rights reserved.
//

import Foundation
import  UIKit

extension UITableView{
    func dequeueReusableCell<T:UITableViewCell>(_ type: T.Type) -> T? {
        var fullName: String = NSStringFromClass(T.self)
        if let range = fullName.range(of: ".", options:NSString.CompareOptions.backwards, range: nil, locale: nil){
            fullName = String(fullName[range.upperBound...])
        }
        return self.dequeueReusableCell(withIdentifier: fullName) as? T
    }
}

extension UIStoryboard {
    func instantiate<T:UIViewController>(_ type: T.Type) -> T? {
        var fullName: String = NSStringFromClass(T.self)
        if let range = fullName.range(of: ".", options:NSString.CompareOptions.backwards, range: nil, locale: nil){
            fullName = String(fullName[range.upperBound...])
        }
        return self.instantiateViewController(identifier:fullName) as? T
    }
}

extension String{
    var attributedTitle:NSAttributedString{
        let titleAttributes = [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: .headline), NSAttributedString.Key.foregroundColor: UIColor.black]
        let titleString = NSMutableAttributedString(string:self, attributes: titleAttributes)
        return titleString
    }
    var attributedSubTitle:NSAttributedString{
        let subtitleAttributes = [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: .subheadline)]
        let subTitleString = NSMutableAttributedString(string:self, attributes:subtitleAttributes)
        return subTitleString
    }
    
    var toDate:Date?{
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZ"
        return dateFormatter.date(from:self)
    }
}
