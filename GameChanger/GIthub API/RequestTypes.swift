//
//  RequestType.swift
//  GameChanger
//
//  Created by Gaurav Luthra on 2/29/20.
//  Copyright © 2020 Gaurav Luthra. All rights reserved.
//

import Foundation

public protocol Request {
    var baseURL: String { get }
    var path: String { get }
    var method: Methods { get }
    var parameters: Any? { get }
    var headers: [[String:String]] { get }
}

public enum Methods: String{
    case get = "GET"
    case post = "POST"
}

public enum RequestTypes{
    case getIssues
    case getComments(String)
}

extension RequestTypes:Request{
    public var baseURL: String{
        return "https://api.github.com/repos/firebase/firebase-ios-sdk"
    }
    
    public var path:String{
        switch self {
        case .getIssues:
            return "/issues"
        case .getComments(let issueId):
            return "/issues/\(issueId)/comments"
        }
    }
  
    public var method: Methods{
        return .get
    }
   
    public var parameters: Any?{
        return nil
    }
  
    public var headers: [[String:String]]{
        return [["Content-Type": "application/json"]]
    }
    
}
