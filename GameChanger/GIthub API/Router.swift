//
//  Router.swift
//  GameChanger
//
//  Created by Gaurav Luthra on 2/29/20.
//  Copyright © 2020 Gaurav Luthra. All rights reserved.
//

import Foundation
import RxSwift

protocol NetworkRouter:class{
    func request<T:Codable>(_ request:RequestTypes,for type:T.Type) -> Observable<[T]>
}

class Router:NetworkRouter{
    func request<T:Codable>(_ request:RequestTypes,for type:T.Type) -> Observable<[T]>{
        let session = URLSession(configuration: URLSessionConfiguration.default)
        return Observable.create { observer in
            guard let urlRequest = self.buildRequest(request) else{
                return Disposables.create()
            }
            let task = session.dataTask(with: urlRequest, completionHandler: { (data, response, error) in
                if let err = error{
                    observer.onError(err)
                    return
                }
                if let dataValue = data{
                    let issues = try! JSONDecoder().decode([T].self, from:dataValue)
                    observer.on(.next(issues))
                }
            })
            task.resume()
            return Disposables.create{
                task.cancel()
            }
        }
    }
}

extension Router {
    private func buildRequest(_ request:RequestTypes) -> URLRequest?{
        guard let url = URL(string: request.baseURL + request.path) else {return nil}
        let urlRequest = URLRequest(url: url)
        return urlRequest
    }
}
