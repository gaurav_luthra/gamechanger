//
//  IssueCommentsTableViewCell.swift
//  GameChanger
//
//  Created by Gaurav Luthra on 2/29/20.
//  Copyright © 2020 Gaurav Luthra. All rights reserved.
//

import UIKit

class IssueCommentsTableViewCell: UITableViewCell {

    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var userImgView: UIImageView!
    @IBOutlet weak var commentTimeLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func configure(comment:Comment?){
        commentLabel.text = comment?.body
        let titleAttributes = [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: .headline), NSAttributedString.Key.foregroundColor: UIColor.black]
        let usernameString = NSMutableAttributedString(string:(comment?.user?.login ?? ""), attributes: titleAttributes)
        usernameString.append(NSAttributedString(string:" commented"))
        commentTimeLabel.attributedText = usernameString
        
        userImgView.layer.cornerRadius = userImgView.frame.size.height/2
        userImgView.clipsToBounds = true
    }

}
