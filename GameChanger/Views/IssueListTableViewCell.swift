//
//  IssueCell.swift
//  GameChanger
//
//  Created by Gaurav Luthra on 2/29/20.
//  Copyright © 2020 Gaurav Luthra. All rights reserved.
//

import UIKit

class IssueCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    override func awakeFromNib(){
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func configure(issue:Issue?){
        setIssueTitle(title:issue?.title)
        setIssueBody(body:issue?.body)
    }
    
    func setIssueTitle(title:String?){
        titleLabel.attributedText = (title ?? "").attributedTitle
    }
    
    func setIssueBody(body:String?){
        subTitleLabel.attributedText = String(body ?? "").attributedSubTitle
    }

}

class IssueListTableViewCell:IssueCell{
    override func setIssueBody(body: String?){
        subTitleLabel.attributedText = String((body ?? "").prefix(140)).attributedSubTitle
    }
}


