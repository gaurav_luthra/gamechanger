//
//  IssueListViewController.swift
//  GameChanger
//
//  Created by Gaurav Luthra on 2/29/20.
//  Copyright © 2020 Gaurav Luthra. All rights reserved.
//

import UIKit
import RxSwift

class IssuesViewController:UIViewController{
    let router = Router()
    let bag = DisposeBag()
}

class IssueListViewController: IssuesViewController,IssuesValidityChecker {
    
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var issueListTableView: IssuesTableView!
    private lazy var issueListVM = IssueListViewModel()
    
    override func viewDidLoad(){
        super.viewDidLoad()
        indicator.center = self.view.center
        let validIssues = getValidIssues()
        if validIssues.isEmpty{
            getAllIssues()
        }else{
            self.issueListVM.addElement(validIssues)
        }
    }
    
    private func getAllIssues(){
        indicator.startAnimating()
        router.request(.getIssues,for:Issue.self).subscribe(
            onNext: { [weak self] (issues) in
                self?.issueListVM.addElement(issues)
                DispatchQueue.main.async{
                    self?.indicator.stopAnimating()
                    self?.issueListTableView.reloadData()
                    CoreDataManager.shared.refreshData(issues:issues)
                }
            },
            onError: {[weak self] error in
                DispatchQueue.main.async{
                    print("Error getting issues:",error)
                    self?.indicator.stopAnimating()
                }
                
            }
        ).disposed(by:bag)
    }
}


extension IssueListViewController:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return issueListVM.numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(IssueListTableViewCell.self)!
        cell.configure(issue:issueListVM.getElement(at:indexPath.row))
        return cell
    }
}

extension IssueListViewController:UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let issue = issueListVM.getElement(at:indexPath.row) else { return }
        let issueDetailCont = storyboard!.instantiate(IssueDetailViewController.self)!
        issueDetailCont.setIssue(issue:issue)
        self.navigationController?.pushViewController(issueDetailCont,animated:true)
    }
}
