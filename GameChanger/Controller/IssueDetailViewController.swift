//
//  IssueDetailViewController.swift
//  GameChanger
//
//  Created by Gaurav Luthra on 2/29/20.
//  Copyright © 2020 Gaurav Luthra. All rights reserved.
//

import UIKit
import RxSwift

class IssueDetailViewController: IssuesViewController {

    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var issueDetailTableView: IssuesTableView!
    private var issueDetailVM:IssueDetailViewModel!
  
    override func viewDidLoad(){
        super.viewDidLoad()
        indicator.center = self.view.center
        getAllComments()
    }
    
    func setIssue(issue:Issue){
        issueDetailVM = IssueDetailViewModel(issue:issue)
    }
    
    private func getAllComments(){
        self.indicator.startAnimating()
        guard let issueNumber = issueDetailVM.issue.number else { return }
        router.request(.getComments(String(issueNumber)),for:Comment.self).subscribe(
            onNext: { [weak self] comments in
                DispatchQueue.main.async{
                    self?.indicator.stopAnimating()
                    if comments.isEmpty{
                        self?.showAlert(mesg:"Comments not available")
                        return
                    }
                    self?.issueDetailVM.addComments(comments)
                    self?.issueDetailTableView.reloadData()
                }
            },
            onError: {[weak self] error in
                DispatchQueue.main.async{
                    self?.indicator.stopAnimating()
                    print("Error loading comments")
                    self?.showAlert(mesg:error.localizedDescription)
                }
            }
        ).disposed(by:bag)
    }
    
    func showAlert(mesg:String){
        let alertController:UIAlertController
        alertController = UIAlertController(title:"", message:mesg, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.cancel) {UIAlertAction in}
        alertController.addAction(cancelAction)
        self.present(alertController,animated: true, completion: nil)
    }
}

extension IssueDetailViewController:UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return issueDetailVM.numSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return issueDetailVM.numberOfrows(at:section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(IssueCell.self)!
            cell.configure(issue:issueDetailVM.issue)
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(IssueCommentsTableViewCell.self)!
            cell.configure(comment:issueDetailVM.commentListVM?.getElement(at:indexPath.row))
            return cell
        }
    }
}


