//
//  Comment.swift
//  GameChanger
//
//  Created by Gaurav Luthra on 2/29/20.
//  Copyright © 2020 Gaurav Luthra. All rights reserved.
//

import Foundation

struct Comment: Codable {
    let url, htmlURL, issueURL: String?
    let id: Int?
    let nodeID: String?
    let user: User?
    let createdAt, updatedAt: Date?
    let authorAssociation, body: String?

    enum CodingKeys: String, CodingKey {
        case url
        case htmlURL
        case issueURL
        case id
        case nodeID
        case user
        case createdAt
        case updatedAt
        case authorAssociation
        case body
    }
}

// MARK: - User
struct User: Codable {
    let login: String?
    let id: Int?
    let nodeID: String?
    let avatarURL: String?
    let gravatarID: String?
    let url, htmlURL, followersURL: String?
    let followingURL, gistsURL, starredURL: String?
    let subscriptionsURL, organizationsURL, reposURL: String?
    let eventsURL: String?
    let receivedEventsURL: String?
    let type: String?
    let siteAdmin: Bool?

    enum CodingKeys: String, CodingKey {
        case login, id
        case nodeID
        case avatarURL
        case gravatarID
        case url
        case htmlURL
        case followersURL
        case followingURL
        case gistsURL
        case starredURL
        case subscriptionsURL
        case organizationsURL
        case reposURL
        case eventsURL
        case receivedEventsURL
        case type
        case siteAdmin
    }
}
