//
//  Issue.swift
//  GameChanger
//
//  Created by Gaurav Luthra on 2/29/20.
//  Copyright © 2020 Gaurav Luthra. All rights reserved.
//

import Foundation


struct Issue: Codable {
    var url, repositoryURL: String?
    var labelsURL: String?
    var commentsURL, eventsURL, htmlURL: String?
    var id: Int?
    var nodeID: String?
    var number: Int64?
    var title: String?
    var state: String?
    var locked: Bool?
    var comments: Int?
    var createdAt, updatedAt: String?
    var authorAssociation, body: String?

    enum CodingKeys: String, CodingKey {
        case url
        case repositoryURL
        case labelsURL
        case commentsURL
        case eventsURL
        case htmlURL
        case id
        case nodeID
        case number, title, state, locked, comments
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case authorAssociation
        case body
    }
}

