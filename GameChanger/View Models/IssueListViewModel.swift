//
//  IssueListViewModel.swift
//  GameChanger
//
//  Created by Gaurav Luthra on 2/29/20.
//  Copyright © 2020 Gaurav Luthra. All rights reserved.
//

import Foundation

protocol ListViewModel{
    associatedtype T
    var isEmpty:Bool { get }
    var numberOfRows:Int { get }
    func getElement(at index:Int) -> T?
    func addElement(_ elements:[T])
    func removeAll()
}

class IssueListViewModel:ListViewModel{
    func removeAll() {
        self.allIssues.removeAll()
    }
    
    typealias T = Issue
    private var allIssues = [T]()
    
    var numberOfRows: Int{
        return allIssues.count
    }
    
    var isEmpty: Bool{
        return allIssues.isEmpty
    }
    
    func getElement(at index:Int) -> T?{
        if index >= allIssues.count || self.isEmpty { return nil }
        return allIssues[index]
    }
    
    func addElement(_ elements:[T]){
        allIssues = elements
        arrange()
    }
    
    private func arrange(){
        allIssues.sort { (aIssue,bIssue) -> Bool in
            if let aDate = aIssue.updatedAt?.toDate,let bDate = bIssue.updatedAt?.toDate,aDate.compare(bDate) == .orderedDescending{
                return true
            }
            return false
        }
    }
}
