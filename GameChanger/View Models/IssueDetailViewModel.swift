//
//  IssueDetailViewModel.swift
//  GameChanger
//
//  Created by Gaurav Luthra on 2/29/20.
//  Copyright © 2020 Gaurav Luthra. All rights reserved.
//

import Foundation

class IssueDetailViewModel{
    private(set) var issue:Issue
    private(set) var commentListVM:CommentListViewModel?
    
    init(issue:Issue){
        self.issue = issue
    }
    func addComments(_ comments:[Comment]){
        self.commentListVM = CommentListViewModel()
        self.commentListVM?.addElement(comments)
    }
    
    var numSections:Int{
        return 2
    }
    
    func numberOfrows(at section:Int) -> Int{
        if section == 0{ return 1 }
        return commentListVM?.numberOfRows ?? 0
    }
}
