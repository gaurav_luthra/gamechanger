//
//  CommentListViewModel.swift
//  GameChanger
//
//  Created by Gaurav Luthra on 2/29/20.
//  Copyright © 2020 Gaurav Luthra. All rights reserved.
//

import Foundation

class CommentListViewModel:ListViewModel{
    func removeAll() {
        self.allComments.removeAll()
    }
    
    typealias T = Comment
    private var allComments = [T]()
    
    var numberOfRows: Int{
        return allComments.count
    }
    
    var isEmpty: Bool{
        return allComments.isEmpty
    }
    
    func getElement(at index:Int) -> T?{
        if index >= allComments.count || self.isEmpty { return nil }
        return allComments[index]
    }
    
    func addElement(_ elements:[T]){
        allComments = elements
    }

}
