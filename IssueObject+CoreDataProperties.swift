//
//  IssueObject+CoreDataProperties.swift
//  GameChanger
//
//  Created by Gaurav Luthra on 2/29/20.
//  Copyright © 2020 Gaurav Luthra. All rights reserved.
//
//

import Foundation
import CoreData


extension IssueObject {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<IssueObject> {
        return NSFetchRequest<IssueObject>(entityName: "IssueObject")
    }

    @NSManaged public var title: String?
    @NSManaged public var body: String?
    @NSManaged public var updated_at: String?
    @NSManaged public var number:Int64

}
