//
//  IssueObjectList+CoreDataProperties.swift
//  GameChanger
//
//  Created by Gaurav Luthra on 2/29/20.
//  Copyright © 2020 Gaurav Luthra. All rights reserved.
//
//

import Foundation
import CoreData


extension IssueObjectList {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<IssueObjectList> {
        return NSFetchRequest<IssueObjectList>(entityName: "IssueObjectList")
    }

    @NSManaged public var savedAt: Int64
    @NSManaged public var issuesData: Data?

}
